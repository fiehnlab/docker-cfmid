#!/bin/bash

# Name of the docker image
IMAGE_NAME="cfmid"
IMAGE_REGISTRY="eros.fiehnlab.ucdavis.edu/$IMAGE_NAME"

# Run docker container
docker run -it -v /root/data:/data:Z $IMAGE_REGISTRY:latest
