# CFM-ID Dockerfile

This repository contains a Dockerfile of [CFM-ID](https://sourceforge.net/projects/cfm-id/) for an automated build on [Docker Hub](https://hub.docker.com/r/ssmehta/cfmid/).  CFM-ID is a collection of utilities "associated with the interpretation of tandem mass spectra (MS/MS) for the purpose of automated metabolite identification: annotation of the peaks in a spectrum for a known chemical structure; prediction of spectra for a given chemical structure and putative metabolite identification."

## Usage

To run a container in interactive mode

    docker run -it ssmehta/cfmid

This will run a container with the most recent revision from the CFM-ID SourceForge repository based on the latest [RDKit](https://hub.docker.com/r/ssmehta/rdkit/) build.  One additional version of the image containing revision 25 of the CFM-ID source based on RDKit Release_2016_03_1 is available and tagged as `r25`:

    docker run -it ssmehta/cfmid:r25

This specific build was used in Blaženović et al. (In Preparation) and is provided to allow for reproducibility of results.

It is possible to mount a host directory or attach data volume to the container's `/data` directory.  The entrypoint script is set up to look for and automatically execute a script at `/data/run.sh` it if it exists.  

## Building

Build with the usual

    docker build -t cfmid .

and then run with

    docker run -it cfmid
