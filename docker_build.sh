#!/bin/bash

# Name of the docker image
IMAGE_NAME="cfmid"
IMAGE_REGISTRY="eros.fiehnlab.ucdavis.edu/$IMAGE_NAME"

# Build docker image
docker build -t $IMAGE_NAME . | tee docker_build.log || exit 1

# Tag the docker container
ID=$(tail -1 docker_build.log | awk '{print $3;}')
docker tag $ID $IMAGE_NAME:latest
docker tag $ID $IMAGE_REGISTRY:latest

# Push if requested
if [ "$1" == "push" ]; then
	echo "Pushing $IMAGE to registry..."
	docker push $IMAGE_REGISTRY
else
	echo "Pushing disabled - use 'push' as an argument to push to the Docker registry"
fi
